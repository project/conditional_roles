<?php
/**
 * @file
 * Admin page callback file for the conditional roles module.
 */

/**
 * Form builder; configure enforced conditions.
 *
 * @ingroup forms
 * @see conditional_roles_admin_conditions_submit()
 */
function conditional_roles_admin_conditions() {
  $form = array();

  $modules = module_implements('conditional_roles_info', TRUE);
  if (empty($modules)) {
    $form['empty'] = array(
      '#markup' => t('None of the currently installed modules define conditions for role assignments.'),
    );
  }

  // Build a table of available conditions for each module.
  $form['conditional_roles_enforced_conditions'] = array(
    '#tree' => TRUE,
  );

  $header = array(
    'label' => t('Condition'),
    'description' => t('Description'),
  );

  $enforced_conditions = variable_get('conditional_roles_enforced_conditions', array());

  foreach ($modules as $module) {
    $module_info = system_get_info('module', $module);
    $form['conditional_roles_enforced_conditions'][$module] = array(
      '#type' => 'fieldset',
      '#title' => check_plain($module_info['name']),
      '#collapsible' => TRUE,
    );

    $conditions = module_invoke($module, 'conditional_roles_info');
    $options = array();
    foreach ($conditions as $cid => $info) {
      $options[$cid] = array(
        'label' => check_plain($info['label']),
        'description' => isset($info['description']) ? filter_xss_admin($info['description']) : '',
      );
    }

    $form['conditional_roles_enforced_conditions'][$module]['conditions'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#empty' => t('No conditions available.'),
      '#default_value' => $enforced_conditions,
    );
  }

  $form = system_settings_form($form);
  // Use our own submission handler to collapse the form tree before saving.
  unset($form['#submit']);

  return $form;
}

/**
 * Submission handler; configure enforced conditions.
 */
function conditional_roles_admin_conditions_submit($form, $form_state) {
  $enforced_conditions = array();
  if (isset($form_state['values']['conditional_roles_enforced_conditions'])) {
    // Get the conditions for each module.
    foreach ($form_state['values']['conditional_roles_enforced_conditions'] as $conditions) {
      foreach ($conditions['conditions'] as $condition => $enforced) {
        if ($enforced) {
          $enforced_conditions[$condition] = TRUE;
        }
      }
    }
  }
  $form_state['values']['conditional_roles_enforced_conditions'] = $enforced_conditions;

  system_settings_form_submit($form, $form_state);

  // Update the status field in the database.
  conditional_roles_synchronize();
}

/**
 * Menu callback; assign conditions to a user's roles.
 *
 * @param $account
 *   An object representing the user whose roles are being edited.
 *
 * @return
 *   Themed output.
 */
function conditional_roles_admin_user($account) {
  $conditional_roles = isset($account->conditional_roles) ? $account->conditional_roles : array();

  $header = array();
  $header['unconditional'] = array(
    t('Role'),
    array(
      'data' => t('Operations'),
      'colspan' => '2',
    ),
  );
  $header['conditional'] = array(
    t('Role'),
    t('Conditions'),
    array(
      'data' => t('Operations'),
      'colspan' => '3',
    ),
  );

  $rows = array(
    'unconditional' => array(),
    'conditional' => array(),
  );
  foreach ($account->roles as $rid => $role_name) {
    if ($rid != DRUPAL_AUTHENTICATED_RID && $rid != DRUPAL_ANONYMOUS_RID) {
      $status = isset($conditional_roles[$rid]) ? 'conditional' : 'unconditional';
      $row = array(check_plain($role_name));

      if ($status == 'conditional') {
        $row[] = conditional_roles_admin_conditions_display($conditional_roles[$rid]);
        $row[] = array('data' => l(t('edit conditions'), 'user/' . $account->uid . '/conditional_roles/manage/' . $rid));
        $row[] = array('data' => l(t('delete all conditions'), 'user/' . $account->uid . '/conditional_roles/manage/' . $rid . '/delete'));
      }
      else {
        $row[] = array('data' => l(t('add conditions'), 'user/' . $account->uid . '/conditional_roles/manage/' . $rid));
      }
      $row[] = array('data' => l(t('revoke role'), 'user/' . $account->uid . '/conditional_roles/manage/' . $rid . '/revoke'));

      $rows[$status][] = $row;
    }
  }

  $unconditional_table = array('header' => $header['unconditional'], 'rows' => $rows['unconditional']);
  $conditional_table = array('header' => $header['conditional'], 'rows' => $rows['conditional']);
  return theme('conditional_roles_admin_user', array('unconditional' => $unconditional_table, 'conditional' => $conditional_table));
}

/**
 * Form builder; assign a user role with conditions.
 */
function conditional_roles_admin_user_add($form, &$form_state, $account) {
  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $account->uid,
  );

  // Get a list of all roles and filter out those that the user already has.
  $roles = array_map('check_plain', user_roles(TRUE));
  foreach (array_keys($account->roles) as $rid) {
    unset($roles[$rid]);
  }

  if (empty($roles)) {
    $form['empty'] = array(
      '#markup' => t('<p>This user is already a member of all defined roles. To add or edit conditions on an existing role, click Cancel and use the "add" and "edit" links. To create a new role, use the <a href="@admin_roles">administer roles page</a>.</p>', array('@admin_roles' => url('admin/people/permissions/roles'))),
    );
  }
  elseif (!isset($form_state['#conditional_roles_add_role'])) {
    // Phase 1 - Select a role to assign.
    $form['role'] = array(
      '#type' => 'select',
      '#title' => t('Assign role to user'),
      '#options' => $roles,
    );
    $form['submit_next'] = array(
      '#type' => 'submit',
      '#value' => t('Next'),
      '#name' => 'next',
    );
  }
  else {
    // Phase 2 - Add conditions to the selected role.
    $role = user_role_load($form_state['#conditional_roles_add_role']);
    $role->name = check_plain($role->name);

    $form['role'] = array(
      '#type' => 'container',
      '#attributes' => array('class' => array('container-inline')),
    );
    $form['role']['selection'] = array(
      '#markup' => t('<dl><dt>Role to assign</dt><dd>@role_name</dd></dl>', array('@role_name' => $role->name)),
    );
    $form['role']['submit_change'] = array(
      '#type' => 'submit',
      '#value' => t('Change role'),
      '#name' => 'change',
    );

    $form = conditional_roles_admin_user_edit($form, $form_state, $account, $role);
  }

  $form['submit_cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#name' => 'cancel',
  );
  return $form;
}

/**
 * Submission handler; assign a user role with conditions.
 */
function conditional_roles_admin_user_add_submit($form, &$form_state) {
  $uid = $form_state['values']['uid'];

  if (isset($form_state['values']['next'])) {
    // Set the role and rebuild the form for phase 2.
    $form_state['#conditional_roles_add_role'] = $form_state['values']['role'];
    $form_state['rebuild'] = TRUE;
  }
  elseif (isset($form_state['values']['cancel'])) {
    // Abort.
    $form_state['redirect'] = 'user/' . $uid . '/conditional_roles';
  }
  elseif (isset($form_state['values']['save'])) {
    // Add the user to the role, then hand off to the 'edit' form submission
    // handler to save the conditions.
    user_multiple_role_edit(array($uid), 'add_role', $form_state['values']['rid']);
    conditional_roles_admin_user_edit_submit($form, $form_state);
  }

  // If we make it here, the 'Change role' button was pressed, so we return
  // nothing to reset the form to phase 1.
}

/**
 * Form builder; edit the conditions on a user's role.
 */
function conditional_roles_admin_user_edit($form, &$form_state, $account, $role) {
  $info = module_invoke_all('conditional_roles_info');
  $conditional_roles = isset($account->conditional_roles) ? $account->conditional_roles : array();
  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $account->uid,
  );
  $form['rid'] = array(
    '#type' => 'value',
    '#value' => $role->rid,
  );

  // Get the current list of applied conditions.
  if (isset($form_state['#conditional_roles_applied'])) {
    $applied_conditions = $form_state['#conditional_roles_applied'];
  }
  else {
    $applied_conditions = isset($conditional_roles[$role->rid]) ? $conditional_roles[$role->rid] : array();
  }

  // Exclude applied conditions from the "add" form.
  $available_conditions = variable_get('conditional_roles_enforced_conditions', array());
  $options = array();
  foreach (array_keys($available_conditions) as $cid) {
    if (!isset($applied_conditions[$cid])) {
      $options[$cid] = check_plain($info[$cid]['label']);
    }
  }

  // Build the "add conditions" form.
  if (!empty($options)) {
    $form['available'] = array(
      '#type' => 'container',
      '#attributes' => array('class' => array('container-inline')),
      '#tree' => TRUE,
    );
    $form['available']['condition'] = array(
      '#type' => 'select',
      '#title' => t('Add a condition'),
      '#options' => $options,
    );
    $form['available']['add'] = array(
      '#type' => 'submit',
      '#value' => t('Add'),
      '#submit' => array('conditional_roles_admin_user_edit_action'),
      '#name' => 'add',
    );
  }
  elseif (empty($available_conditions)) {
    $form['available'] = array(
      '#markup' => t('No conditions have been made available. Use the <a href="@conditional_roles">Conditional roles administration page</a> to enable conditions for role assignment.', array('@conditional_roles' => url('admin/config/people/conditional_roles'))),
    );
  }

  // Build the "applied conditions" form.
  $form['applied'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
  );

  foreach ($applied_conditions as $cid => $assigned_values) {
    $form['applied'][$cid] = array(
      '#type' => 'fieldset',
      '#title' => check_plain($info[$cid]['label']),
      '#description' => isset($info[$cid]['description']) ? filter_xss_admin($info[$cid]['description']) : '',
    );

    // Get the allowed values for the condition.
    $callback = $info[$cid]['value callback'];
    $options = $callback($cid);
    foreach ($options as $key => $label) {
      $options[$key] = field_filter_xss($label);
    }

    $form['applied'][$cid]['values'] = array(
      '#title' => t('Allowed values'),
      '#type' => 'select',
      '#options' => $options,
      '#multiple' => TRUE,
      '#default_value' => $assigned_values,
    );
    $form['applied'][$cid]['actions']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset'),
      '#submit' => array('conditional_roles_admin_user_edit_action'),
      '#name' => 'reset_' . $cid,
    );
    $form['applied'][$cid]['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('conditional_roles_admin_user_edit_action'),
      '#name' => 'delete_' . $cid,
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save conditions'),
    '#name' => 'save',
  );
  $form['submit_cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#name' => 'cancel',
  );
  return $form;
}

/**
 * Submission handler; actions for editing conditions on a user's role.
 */
function conditional_roles_admin_user_edit_action($form, &$form_state) {
  $name = $form_state['triggering_element']['#name'];
  $applied = isset($form_state['values']['applied']) ? $form_state['values']['applied'] : array();

  // Check which button was clicked.
  if ($name == 'add') {
    // Add the condition to the list of applied conditions.
    $cid = $form_state['values']['available']['condition'];
    $applied[$cid] = array('values' => array());
  }
  elseif (($split = strpos($name, '_')) !== FALSE) {
    $op = drupal_substr($name, 0, $split);
    $cid = drupal_substr($name, $split + 1);

    switch ($op) {
      case 'reset':
        $account = user_load($form_state['values']['uid']);
        $rid = $form_state['values']['rid'];

        // If the role already had this condition, reset to the saved values.
        if (isset($account->conditional_roles) && isset($account->conditional_roles[$rid]) && isset($account->conditional_roles[$rid][$cid])) {
          $form_state['input']['applied'][$cid]['values'] = $applied[$cid] = $account->conditional_roles[$rid][$cid];
        }
        else {
          // Otherwise, reset to none selected.
          $form_state['input']['applied'][$cid]['values'] = $applied[$cid] = array();
        }
        break;

      case 'delete':
        unset($applied[$cid]);
        break;
    }
  }

  $form_state['#conditional_roles_applied'] = $applied;
  $form_state['rebuild'] = TRUE;
}

/**
 * Submission handler; edit the conditions on a user's role.
 */
function conditional_roles_admin_user_edit_submit($form, &$form_state) {
  $uid = $form_state['values']['uid'];
  if (isset($form_state['values']['save'])) {
    $rid = $form_state['values']['rid'];
    // Replace the conditions on this role with those from the form.
    conditional_roles_delete_multiple(array('uid' => $uid, 'rid' => $rid));
    if (isset($form_state['values']['applied'])) {
      foreach ($form_state['values']['applied'] as $cid => $data) {
        conditional_roles_save($uid, $rid, $cid, $data['values']);
      }
    }
  }
  $form_state['redirect'] = 'user/' . $uid . '/conditional_roles';
}

/**
 * Form builder; delete the conditions on a user's role.
 *
 * @param $form
 *   An associative array representing the form.
 * @param $form_state
 *   An associative array representing the current state of the form.
 * @param $account
 *   An object representing a user.
 * @param $role
 *   An object representing a role.
 *
 * @return
 *   An associative array representing the form.
 */
function conditional_roles_admin_user_delete($form, &$form_state, $account, $role) {
  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $account->uid,
  );
  $form['rid'] = array(
    '#type' => 'value',
    '#value' => $role->rid,
  );

  return confirm_form($form,
    t('Are you sure you want to delete all conditions on %role_name?', array('%role_name' => $role->name)),
    'user/' . $account->uid . '/conditional_roles',
    t('This will make %role_name an unconditional role for %user_name.', array('%role_name' => $role->name, '%user_name' => $account->name)),
    t('Delete all conditions'),
    t('Cancel')
  );
}

/**
 * Submission handler; delete the conditions on a user's role.
 */
function conditional_roles_admin_user_delete_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    conditional_roles_delete_multiple(array(
      'uid' => $form_state['values']['uid'],
      'rid' => $form_state['values']['rid'],
    ));
    $account = user_load($form_state['values']['uid']);
    $role = user_role_load($form_state['values']['rid']);
    watchdog('conditional_roles', '@user: removed all conditions on @role.', array('@user' => $account->name, '@role' => $role->name));
    drupal_set_message(t('@role is now an unconditional role for @user.', array('@role' => $role->name, '@user' => $account->name)));
  }

  $form_state['redirect'] = 'user/' . $account->uid . '/conditional_roles';
}

/**
 * Form builder; revoke a user's membership in role.
 *
 * @param $form
 *   An associative array representing the form.
 * @param $form_state
 *   An associative array representing the current state of the form.
 * @param $account
 *   An object representing a user.
 * @param $role
 *   An object representing a role.
 *
 * @return
 *   An associative array representing the form.
 */
function conditional_roles_admin_user_revoke($form, &$form_state, $account, $role) {
  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $account->uid,
  );
  $form['rid'] = array(
    '#type' => 'value',
    '#value' => $role->rid,
  );

  return confirm_form($form,
    t('Are you sure you want to revoke membership in %role_name?', array('%role_name' => $role->name)),
    'user/' . $account->uid . '/conditional_roles',
    t('This will remove the %role_name role from %user_name.', array('%role_name' => $role->name, '%user_name' => $account->name)),
    t('Revoke role'),
    t('Cancel')
  );
}

/**
 * Submission handler; revoke a user's membership in role.
 */
function conditional_roles_admin_user_revoke_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    user_multiple_role_edit(array($form_state['values']['uid']), 'remove_role', $form_state['values']['rid']);
    $account = user_load($form_state['values']['uid']);
    $role = user_role_load($form_state['values']['rid']);
    drupal_set_message(t('Membership in @role has been revoked for @user.', array('@role' => $role->name, '@user' => $account->name)));
  }

  $form_state['redirect'] = 'user/' . $account->uid . '/conditional_roles';
}

/**
 * Outputs a list of conditions and assigned values for a user's role.
 *
 * @param $conditions
 *   An associative array keyed by condition ID where each value is an array of
 *   assigned values for that condition.
 *
 * @return
 *   A themed list of conditions.
 */
function conditional_roles_admin_conditions_display($conditions) {
  if (empty($conditions)) {
    return;
  }

  $info = module_invoke_all('conditional_roles_info');
  $properties = array();
  foreach ($conditions as $cid => $assigned_values) {
    if (empty($assigned_values)) {
      $values = NULL;
    }
    else {
      $callback = $info[$cid]['value callback'];
      $values = array_map('field_filter_xss', $callback($cid, $assigned_values));
    }

    $properties[$cid] = array(
      'label' => check_plain($info[$cid]['label']),
      'description' => isset($info[$cid]['description']) ? filter_xss_admin($info[$cid]['description']) : '',
      'values' => $values,
    );
  }

  return theme('conditional_roles_admin_conditions', array('conditions' => $properties));
}

/**
 * Returns HTML for a list of conditions and their allowed values.
 *
 * @param $variables
 *   An associative array containing:
 *   - conditions: An associative array containing the properties of one or more
 *     conditions, keyed by condition ID.  Properties used: label, description,
 *     values.
 *
 * @ingroup themeable
 */
function theme_conditional_roles_admin_conditions($variables) {
  $output = '<ul>';
  foreach ($variables['conditions'] as $properties) {
    $output .= '<li>' . theme('conditional_roles_admin_condition', $properties) . '</li>';
  }
  $output .= '</ul>';
  return $output;
}

/**
 * Returns HTML for a condition and its allowed values.
 *
 * @param $variables
 *   An associative array containing:
 *   - label: The condition label.
 *   - description: The condition description.
 *   - values: An associative array of assigned values, where the keys are the
 *     stored values and the values are the displayed values.
 *
 * @ingroup themeable
 */
function theme_conditional_roles_admin_condition($variables) {
  $output = $variables['label'];
  $values = isset($variables['values']) ? implode(', ', $variables['values']) : t('(null)');
  $output .= '<div class="description">' . t('Allowed values:') . ' ' . $values . '</div>';
  return $output;
}

/**
 * Returns HTML for the admin user page.
 *
 * @param $variables
 *   An associative array containing:
 *   - unconditional: An associative array containing the header and rows for a
 *     table of unconditional roles.
 *   - conditional: An associative array containing the header and rows for a
 *     table of conditional roles.
 *
 * @ingroup themeable
 */
function theme_conditional_roles_admin_user($variables) {
  $output = '<h2>' . t('Unconditional roles') . '</h2>';
  if (empty($variables['unconditional']['rows'])) {
    $output .= '<p>' . t('No unconditional roles have been assigned.') . '</p>';
  }
  else {
    $output .= theme('table', $variables['unconditional']);
  }

  $output .= '<h2>' . t('Conditional roles') . '</h2>';
  if (empty($variables['conditional']['rows'])) {
    $output .= '<p>' . t('No conditional roles have been assigned.') . '</p>';
  }
  else {
    $output .= theme('table', $variables['conditional']);
  }
  return $output;
}
