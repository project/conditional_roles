<?php
/**
 * @file
 * API documentation file for Conditional Roles.
 *
 * Note that Conditional Roles implements hook_hook_info().
 * Your module may place its hooks inside a file named
 * $module.conditional_roles.inc for auto-loading by Drupal.
 *
 * Required hooks:
 * - hook_conditional_roles_info()
 *
 * Modules providing condition checks should also implement
 * hook_form_BASE_FORM_ID_alter() to control access on values in the node form.
 *
 * @see conditional_roles_taxonomy_form_node_form_alter()
 */

/**
 * Exposes module-defined node-related data for use by Conditional Roles.
 *
 * Modules that define information on or about nodes may implement this hook to
 * make that information available to Conditional Roles for access checks.
 *
 * @return
 * An associative array of condition checks. The keys of the array are the IDs
 * of the condition checks provided by the module, and each corresponding value
 * is an associative array with the following key-value pairs:
 *
 * - 'label': The human-readable name of the condition, which should be passed
 *   through the t() function for translation.
 * - 'description': An optional extended description of the condition, which
 *   should also be passed through the t() function.
 * - 'check callback': A callback function that returns an array of values that
 *   match the condition check for a given node.  Returned values must be of a
 *   data type that can be used as an array key.
 * - 'value callback': A callback function that returns an array of all possible
 *   values for the condition.  For example, if the condition checks a field,
 *   this would return the list of allowed values for that field.  The returned
 *   array uses the format key => label, where key is the stored value and label
 *   is the display value.  The value callback takes two parameters:
 *   - 'cid': The condition ID for which to return values.
 *   - 'values': An optional array of key values by which to filter the result.
 *     This is used by the administrative UI to avoid unnecessary loads when
 *     displaying the list of values for which a user has been granted a role.
 *
 * When a condition is checked, the check callback will be executed as follows:
 *
 * $callback($node, $cid)
 *
 * For example, to check the 'taxonomy_term' condition (defined by the
 * conditional_roles_taxonomy module), Conditional Roles executes:
 *
 * conditional_roles_taxonomy_check_term($node, 'taxonomy_term');
 *
 * The returned value will be an array of IDs for all taxonomy terms associated
 * with the node.  Conditional Roles will then compare that array with the array
 * of allowed values on the user's conditional role assignment to determine the
 * user's permissions.
 */
function hook_conditional_roles_info() {
  $info = array();

  // Support generic term associations, regardless of field.
  $info['taxonomy_term'] = array(
    'label' => t('The node is associated with a term'),
    'description' => t('This condition requires the taxonomy module to be maintaining the taxonomy_index table, and does not work with unpublished nodes.'),
    'check callback' => 'conditional_roles_taxonomy_check_term',
    'value callback' => 'conditional_roles_taxonomy_values',
  );

  // Support all active taxonomy_term_reference fields.
  $fields = field_read_fields(array('type' => 'taxonomy_term_reference'));
  foreach ($fields as $name => $field) {
    $info[$name] = array(
      'label' => t('!field contains a term', array('!field' => $name)),
      'check callback' => 'conditional_roles_taxonomy_check_term_reference',
      'value callback' => 'conditional_roles_taxonomy_values',
    );
  }

  return $info;
}
