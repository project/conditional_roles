<?php
/**
 * @file
 * The Conditional Roles module.
 *
 * Conditional Roles provides a means of associating users to roles by condition.
 * Roles are NOT dynamically assigned to users (i.e. role escalation); rather,
 * the user must already be assigned to a role, and then Conditional Roles
 * determines where the user can exercise the permissions granted by that role.
 *
 * For example, consider a news site with the following roles:
 *
 *  - contributors have permission to "create articles" and "edit own articles"
 *  - editors have permission to "edit any articles"
 *
 * Articles are assigned to sections by a taxonomy term, i.e. Sports, Arts, etc.
 *
 * Now, assume that user Sally needs to be an editor for the Sports section, but
 * only a contributor for the Arts section.  Under a traditional permissions
 * model, such as with the Taxonomy Access Control module, you would need to
 * create separate roles for each department (i.e. Sports contributors, Sports
 * editors, Arts contributors, and Arts editors), which is fine when you only
 * have a few departments, but quickly becomes unwieldy with many.
 *
 * With Conditional Roles, you would instead assign Sally to both the
 * contributors and editors roles at the site-level, then limit her to only use
 * her editor permissions on articles tagged Sports, and her contributor
 * permissions for articles tagged Arts.  The module also alters the node
 * creation form so that Sally can only select the Arts term when creating new
 * content.
 */

/**
 * Implements hook_menu().
 */
function conditional_roles_menu() {
  $items = array();

  // Configuration pages.
  $items['admin/config/people/conditional_roles'] = array(
    'title' => 'Conditional roles',
    'description' => 'Choose which conditions can be used for conditional role assignments.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('conditional_roles_admin_conditions'),
    'access arguments' => array('administer users'),
    'file' => 'conditional_roles.admin.inc',
  );
  $items['admin/config/people/conditional_roles/conditions'] = array(
    'title' => 'Conditions',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );

  // User administration pages.
  $items['user/%user/conditional_roles'] = array(
    'title' => 'Conditional roles',
    'page callback' => 'conditional_roles_admin_user',
    'page arguments' => array(1),
    'access callback' => 'conditional_roles_user_edit_access',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
    'file' => 'conditional_roles.admin.inc',
    'weight' => 1,
  );
  $items['user/%user/conditional_roles/add'] = array(
    'title' => 'Assign a new role',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('conditional_roles_admin_user_add', 1),
    'access callback' => 'conditional_roles_user_edit_access',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_ACTION,
    'file' => 'conditional_roles.admin.inc',
  );
  $items['user/%user/conditional_roles/manage/%user_role'] = array(
    'title' => 'Manage role conditions',
    'title callback' => 'conditional_roles_user_role_title',
    'title arguments' => array(4),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('conditional_roles_admin_user_edit', 1, 4),
    'access callback' => 'conditional_roles_user_role_access',
    'access arguments' => array(1, 4),
    'file' => 'conditional_roles.admin.inc',
  );
  $items['user/%user/conditional_roles/manage/%user_role/edit'] = array(
    'title' => 'Manage conditions',
    'weight' => -10,
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );
  $items['user/%user/conditional_roles/manage/%user_role/delete'] = array(
    'title' => 'Delete all conditions',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('conditional_roles_admin_user_delete', 1, 4),
    'access callback' => 'conditional_roles_user_role_access',
    'access arguments' => array(1, 4),
    'type' => MENU_LOCAL_TASK,
    'file' => 'conditional_roles.admin.inc',
  );
  $items['user/%user/conditional_roles/manage/%user_role/revoke'] = array(
    'title' => 'Revoke role',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('conditional_roles_admin_user_revoke', 1, 4),
    'access callback' => 'conditional_roles_user_role_access',
    'access arguments' => array(1, 4),
    'type' => MENU_LOCAL_TASK,
    'file' => 'conditional_roles.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_admin_paths().
 */
function conditional_roles_admin_paths() {
  return array(
    'user/*/conditional_roles' => TRUE,
    'user/*/conditional_roles/*' => TRUE,
  );
}

/**
 * Implements hook_hook_info().
 */
function conditional_roles_hook_info() {
  $hooks['conditional_roles_info'] = array(
    'group' => 'conditional_roles',
  );
  return $hooks;
}

/**
 * Implements hook_help().
 */
function conditional_roles_help($path, $arg) {
  switch ($path) {
    // Main module help.
    case 'admin/help#conditional_roles':
      return '<p>' . t('A conditional role is a role that is granted to a user only for nodes that have certain properties.  For example, a conditional role could grant "edit" permissions to a user only for nodes that have been published.  The <a href="@config">conditional roles configuration page</a> lets you choose which node properties can be used as conditions.  To grant a conditional role to a user, assign the role to the user normally, then edit the user and use the "Conditional roles" tab to add conditions to restrict the role assignment.', array('@config' => url('admin/config/people/conditional_roles'))) . '</p>';

    // The condition enforcement configuration form.
    case 'admin/config/people/conditional_roles':
      return '<p>' . t('This page lets you choose which conditions to enforce for conditional role assignments.  If you enable a condition here, you will be able to assign roles to users based on the value of that condition.  For example, if you were to enable the "Node is published" condition, you could then grant a user permission to edit only published nodes.') . '</p>' . '<p>' . t('If you disable a condition here, any role assignments that were granted on the basis of that condition will be treated as unconditional roles (i.e. the role will apply universally) until the condition is re-enabled.') . '</p>';

    // The condition assignment form for a user.
    case 'user/%/conditional_roles':
      return '<p>' . t('Use this screen to assign conditions to this user\'s role assignments. The permissions granted by a <em>conditional role</em> can only be used on nodes that match the specified conditions.  Roles with multiple conditions will apply if any one of the conditions matches the node.  Permissions granted by an <em>unconditional role</em> can be used on any node.') . '</p>';
  }
}

/**
 * Implements hook_theme().
 */
function conditional_roles_theme() {
  return array(
    'conditional_roles_admin_conditions' => array(
      'file' => 'conditional_roles.admin.inc',
      'variables' => array('conditions' => NULL),
    ),
    'conditional_roles_admin_condition' => array(
      'file' => 'conditional_roles.admin.inc',
      'variables' => array('label' => NULL, 'description' => NULL, 'values' => NULL),
    ),
    'conditional_roles_admin_user' => array(
      'file' => 'conditional_roles.admin.inc',
      'variables' => array('unconditional' => NULL, 'conditional' => NULL),
    ),
  );
}

/**
 * Implements hook_modules_disabled().
 */
function conditional_roles_modules_disabled($modules) {
  conditional_roles_synchronize();
}

/**
 * Implements hook_user_load().
 */
function conditional_roles_user_load($users) {
  $result = conditional_roles_load_multiple(array_keys($users), array('status' => 1), array('uid', 'rid', 'cid'));
  foreach ($result as $record) {
    if (!isset($users[$record->uid]->conditional_roles)) {
      $users[$record->uid]->conditional_roles = array();
    }
    $users[$record->uid]->conditional_roles[$record->rid][$record->cid] = $record->allowed;
  }
}

/**
 * Implements hook_user_update().
 */
function conditional_roles_user_update(&$edit, $account, $category) {
  // Clear any associated conditions when a user loses a role.
  if (isset($edit['roles']) && !empty($account->conditional_roles)) {
    $query = db_delete('conditional_roles');
    $query->condition('uid', $account->uid);
    if (!empty($edit['roles'])) {
      $query->condition('rid', array_keys($edit['roles']), 'NOT IN');
    }
    $query->execute();
  }
}

/**
 * Implements hook_user_delete().
 */
function conditional_roles_user_delete($account) {
  return conditional_roles_delete_multiple(array('uid' => $account->uid));
}

/**
 * Implements hook_user_role_delete().
 */
function conditional_roles_user_role_delete($role) {
  return conditional_roles_delete_multiple(array('rid' => $role->rid));
}

/**
 * Implements hook_node_access().
 */
function conditional_roles_node_access($node, $op, $account) {
  // The is_string condition here will probably never be run, since $node should
  // only ever be a string when we're doing a 'create', but we'll leave it to be
  // safe, in case the op list changes in future versions of core.
  if ($op == 'create' || is_string($node)) {
    // We ignore 'create' because there is no way to tell at this stage whether
    // a conditional permission applies to the node or not. Instead, modules
    // that provide condition checks for conditional_roles should implement
    // hook_form_BASE_FORM_ID_alter() to filter disallowed values.
    return NODE_ACCESS_IGNORE;
  }

  // Make sure we have a full user object, in case node_access() passed in the
  // global $user, which doesn't get populated by hook_user_load().
  if (!isset($account->conditional_roles)) {
    $populated_account = user_load($account->uid);
    $account->conditional_roles = isset($populated_account->conditional_roles) ? $populated_account->conditional_roles : array();
  }

  // If none of the user's roles were assigned conditionally, we're done.
  if (empty($account->conditional_roles)) {
    return NODE_ACCESS_IGNORE;
  }

  // Get test values from the node.
  $condition_values = conditional_roles_check_conditions($node);

  switch ($op) {
    case 'view':
      // @todo Add logic for managing view permissions.
      //   see workbench_moderation_node_access() for view of unpublished nodes
      return NODE_ACCESS_IGNORE;

    case 'update':
    case 'delete':
      // @todo Make update and delete aware of revisioning.
      $type = $node->type;
      if (in_array($type, node_permissions_get_configured_types())) {
        $op_perm = ($op == 'delete') ? $op : 'edit';
        if (conditional_roles_user_access($condition_values, "$op_perm any $type content", $account) || (conditional_roles_user_access($condition_values, "$op_perm own $type content", $account) && ($account->uid == $node->uid))) {
          // Using IGNORE instead of ALLOW ensures that we don't allow any
          // operation that would have failed if roles weren't conditional.
          // Thus, the operation should be allowed by node_node_access().
          return NODE_ACCESS_IGNORE;
        }
        // If we found no permissions for the user, explicitly deny the op in
        // order to override any NODE_ACCESS_ALLOW that may have been set by
        // other hook_node_access() implementations, such as node_node_access().
        return NODE_ACCESS_DENY;
      }
      // Ignore node types for which no permissions are defined.
      return NODE_ACCESS_IGNORE;
  }
}

/**
 * Determines whether the user has a given privilege for a set of conditions.
 *
 * @param $condition_values
 *   An array keyed by condition ID where each value is an array of values that
 *   match the condition, as returned by conditional_roles_check_conditions().
 * @param $string
 *   The permission to check, such as "create article".
 * @param $account
 *   (optional) The account to check; if not given use currently logged in user.
 *   Note that this is assumed to be a fully populated user object, as returned
 *   by user_load(). Thus, the global $user should not be explicitly passed to
 *   this function, since it does not pass through hook_user_load(); rather,
 *   exclude this parameter and allow the function to load an object for $user.
 *
 * @return
 *   Boolean TRUE if the user has the requested permission.
 *
 * @see conditional_roles_check_conditions()
 * @see user_access()
 */
function conditional_roles_user_access($condition_values, $string, $account = NULL) {
  global $user;

  if (!isset($account)) {
    // The global $user doesn't pass through hook_user_load(), so we need to
    // load the full user object to populate $account->conditional_roles.
    $account = user_load($user->uid);
  }

  // User #1 has all privileges:
  if ($account->uid == 1) {
    return TRUE;
  }

  // Like user_access(), cache the user's conditional permissions using the
  // advanced drupal_static() pattern to reduce the number of database queries.
  static $drupal_static_fast;
  if (!isset($drupal_static_fast)) {
    $drupal_static_fast['perm'] = &drupal_static(__FUNCTION__);
  }
  $perm = &$drupal_static_fast['perm'];
  if (!isset($perm[$account->uid])) {
    $role_permissions = user_role_permissions($account->roles);

    $perms = array(
      'conditional' => array(),
      'unconditional' => array(),
    );
    foreach ($role_permissions as $rid => $permissions) {
      // Check whether the role was granted conditionally.
      if (isset($account->conditional_roles) && isset($account->conditional_roles[$rid])) {
        foreach ($account->conditional_roles[$rid] as $cid => $allowed_values) {
          if (!isset($perms['conditional'][$cid])) {
            $perms['conditional'][$cid] = array();
          }
          // Add the role's permissions for each allowed value of the condition.
          // This allows for the possibility of multiple roles applying to the
          // same condition value.
          foreach ($allowed_values as $value) {
            if (!isset($perms['conditional'][$cid][$value])) {
              $perms['conditional'][$cid][$value] = array();
            }
            $perms['conditional'][$cid][$value] += $permissions;
          }
        }
      }
      else {
        $perms['unconditional'] += $permissions;
      }
    }
    $perm[$account->uid] = $perms;
  }

  // Check if the user holds the permission in an unconditional role.
  if (isset($perm[$account->uid]['unconditional'][$string])) {
    return TRUE;
  }

  // Check each condition value to see if it matches a conditional permission.
  foreach ($condition_values as $test_cid => $test_values) {
    if (isset($perm[$account->uid]['conditional'][$test_cid])) {
      foreach ($test_values as $test_value) {
        if (isset($perm[$account->uid]['conditional'][$test_cid][$test_value]) && isset($perm[$account->uid]['conditional'][$test_cid][$test_value][$string])) {
          return TRUE;
        }
      }
    }
  }

  return FALSE;
}

/**
 * Returns conditional test values for a node.
 *
 * @param $node
 *   The node.
 *
 * @return
 *   An associative array keyed by condition ID where each value is an array of
 *   values that match the condition for the given node.
 */
function conditional_roles_check_conditions($node) {
  $values = array();
  $conditions = variable_get('conditional_roles_enforced_conditions', array());
  $info = module_invoke_all('conditional_roles_info');

  foreach (array_keys($conditions) as $cid) {
    $callback = $info[$cid]['check callback'];
    $values[$cid] = $callback($node, $cid);
  }
  return $values;
}

/**
 * Load multiple conditional role objects based on certain query conditions.
 *
 * @param $uids
 *   An array of user IDs.
 * @param $conditions
 *   An associative array of query conditions on the {conditional_roles} table,
 *   where the keys are the database fields and the values are the values those
 *   fields must have.
 * @param $order
 *   (optional) An array of fields by which to sort the result set, in order of
 *   precedence. Each field will be sorted in ascending (ASC) order.
 *   For example, array('uid', 'rid') will sort by uid, then by rid.
 *
 * @return
 *   An array of conditional role objects.
 */
function conditional_roles_load_multiple($uids = array(), $conditions = array(), $order = array()) {
  $query = db_select('conditional_roles', 'cr');
  $query->addTag('conditional_roles_load_multiple');
  // Only allow conditional roles for roles the user already has.
  $query->join('users_roles', 'ur', 'cr.uid = ur.uid AND cr.rid = ur.rid');
  $query->fields('cr', array('uid', 'rid', 'cid', 'allowed', 'status'));

  if (!empty($uids)) {
    $query->condition('cr.uid', $uids, 'IN');
  }
  if (!empty($conditions)) {
    foreach ($conditions as $field => $value) {
      $query->condition('cr.' . $field, $value);
    }
  }
  if (!empty($order)) {
    foreach ($order as $field) {
      $query->orderBy($field);
    }
  }

  $conditional_roles = $query->execute()->fetchAll();
  foreach (array_keys($conditional_roles) as $key) {
    $conditional_roles[$key]->allowed = unserialize($conditional_roles[$key]->allowed);
  }
  return $conditional_roles;
}

/**
 * Load all conditional roles for a user.
 *
 * @param $uid
 *   Integer specifying the user ID.
 * @param $order
 *   (optional) An array of fields by which to sort the result set, in order of
 *   precedence. Each field will be sorted in ascending (ASC) order.
 *   For example, array('uid', 'rid') will sort by uid, then by rid.
 *
 * @return
 *   An array of conditional role objects.
 */
function conditional_roles_load_by_user($uid, $order = array()) {
  return conditional_roles_load_multiple(array($uid), array(), $order);
}

/**
 * Load all conditional assignments for a role.
 *
 * @param $rid
 *   Integer specifying the role ID.
 * @param $order
 *   (optional) An array of fields by which to sort the result set, in order of
 *   precedence. Each field will be sorted in ascending (ASC) order.
 *   For example, array('uid', 'rid') will sort by uid, then by rid.
 *
 * @return
 *   An array of conditional role objects.
 */
function conditional_roles_load_by_role($rid, $order = array()) {
  return conditional_roles_load_multiple(array(), array('rid' => $rid), $order);
}

/**
 * Add or update a conditional role assignment.
 *
 * @param $uid
 *   A user ID.
 * @param $rid
 *   A role ID.
 * @param $cid
 *   A condition ID.
 * @param $allowed
 *   (optional) An array of allowed values for the condition.
 * @param $status
 *   (optional) A boolean indicating whether the assignment is active. Defaults
 *   to TRUE.
 *
 * @return
 *   Status constant indicating if the conditional role was inserted or updated.
 */
function conditional_roles_save($uid, $rid, $cid, $allowed = array(), $status = 1) {
  // Check that the user is a member of the role, and that the condition exists.
  $account = user_load($uid);
  $conditions = module_invoke_all('conditional_roles_info');
  if ($account !== FALSE && isset($account->roles[$rid]) && isset($conditions[$cid]) && $rid != DRUPAL_AUTHENTICATED_RID && $rid != DRUPAL_ANONYMOUS_RID) {
    return db_merge('conditional_roles')
      ->key(array('uid' => $uid, 'rid' => $rid, 'cid' => $cid))
      ->fields(array(
        'allowed' => serialize($allowed),
        'status' => $status,
      ))
      ->execute();
  }
  return FALSE;
}

/**
 * Deletes multiple conditional role records based on certain query conditions.
 *
 * @param $conditions
 *   An associative array of query conditions on the {conditional_roles} table,
 *   where the keys are the database fields and the values are the values those
 *   fields must have.
 *
 * @return
 *   The number of records deleted.
 */
function conditional_roles_delete_multiple($conditions) {
  // Don't allow an unconditional deletion of all records.
  if (empty($conditions)) {
    return;
  }

  $query = db_delete('conditional_roles');
  foreach ($conditions as $field => $value) {
    $query->condition($field, $value);
  }
  return $query->execute();
}

/**
 * Deletes a conditional role assignment.
 *
 * @param $uid
 *   A user ID.
 * @param $rid
 *   A role ID.
 * @param $cid
 *   A condition ID.
 *
 * @return
 *   The number of records deleted (should always be either 0 or 1).
 */
function conditional_roles_delete($uid, $rid, $cid) {
  return conditional_roles_delete_multiple(array('uid' => $uid, 'rid' => $rid, 'cid' => $cid));
}

/**
 * Update the status of conditional roles based on current enforced conditions.
 *
 * This should be called any time the conditional_roles_enforced_conditions
 * variable is changed.
 */
function conditional_roles_synchronize() {
  // Remove any conditions that are no longer provided by an enabled module.
  $info = module_invoke_all('conditional_roles_info');
  $enforced_conditions = variable_get('conditional_roles_enforced_conditions', array());
  foreach (array_keys($enforced_conditions) as $cid) {
    if (!isset($info[$cid])) {
      unset($enforced_conditions[$cid]);
    }
  }
  variable_set('conditional_roles_enforced_conditions', $enforced_conditions);

  // Check whether any conditions are enforced.
  if (empty($enforced_conditions)) {
    // Disable all conditional assignments.
    db_update('conditional_roles')
      ->fields(array(
        'status' => 0,
      ))
      ->execute();
  }
  else {
    $cids = array_keys($enforced_conditions);

    // Enable all enforced conditions.
    db_update('conditional_roles')
      ->fields(array(
        'status' => 1,
      ))
      ->condition('cid', $cids, 'IN')
      ->execute();
    // Disable all unenforced conditions.
    db_update('conditional_roles')
      ->fields(array(
        'status' => 0,
      ))
      ->condition('cid', $cids, 'NOT IN')
      ->execute();
  }
}

/**
 * Menu title callback; manage role conditions.
 *
 * @param $role
 *   A role object.
 */
function conditional_roles_user_role_title($role) {
  return $role->name;
}

/**
 * Menu access callback; manage role conditions.
 *
 * @param $user
 *   A user object.
 * @param $role
 *   A role object.
 */
function conditional_roles_user_role_access($account, $role) {
  return conditional_roles_user_edit_access($account) && isset($account->roles[$role->rid]);
}

/**
 * Menu access callback; manage a user's conditional roles.
 */
function conditional_roles_user_edit_access($account) {
  return user_access('administer users') && user_access('administer permissions') && $account->uid > 0;
}
